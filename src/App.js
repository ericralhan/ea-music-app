import React from 'react';
import './App.css';
import RecordsContainer from './components/RecordsContainer';

class App extends React.Component {
  render() {
    return (
      <>
        <RecordsContainer></RecordsContainer>
      </>
    );
  }
}

export default App;
