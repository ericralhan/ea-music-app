import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

/**
 * Records is a view level component that renders records in below format
 *
 * {
 *    abc: {
 *      def: {
 *        ghi: {
 *          jkl: {
 *            .. so on, until it finds below child:
 *            mno: null
 *          }
 *        }
 *      }
 *    }
 * }
 *
 * @class Records
 * @extends {PureComponent}
 */
class Records extends PureComponent {
  static propTypes = {
    records: PropTypes.object.isRequired
  };

  renderObject(object) {
    let keys = Object.keys(object).sort();

    return (
      <ul>
        {keys.map((key, index) => {
          let subObject = object[key];
          let renderNext = subObject ? true : false;
          return (
            <li key={index}>
              {key}
              {renderNext && this.renderObject(subObject)}
            </li>
          );
        })}
      </ul>
    );
  }

  render() {
    const { records } = this.props;

    const isRecordsEmpty =
      Object.keys(records).length === 0 && records.constructor === Object;

    return (
      <>
        <div>Record list:</div>
        {isRecordsEmpty && <div>No records to display</div>}
        {this.renderObject(records)}
      </>
    );
  }
}

export { Records };
