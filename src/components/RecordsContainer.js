import React, { Component } from 'react';
import { Records } from './Records';
import { FestivalsApi } from '../api/FestivalsApi';
import { RecordsTransform } from '../helper/RecordsTransform';

/**
 * Records container, arguably the heart of this small app
 *
 * @class RecordsContainer
 * @extends {Component}
 */
class RecordsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      festivals: []
    };
  }

  componentDidMount() {
    FestivalsApi.get().then(result => {
      this.setState({
        festivals: result
      });
    });
  }

  render() {
    const { festivals } = this.state;
    let records = RecordsTransform.from(festivals);

    return (
      <>
        <Records records={records} />
      </>
    );
  }
}

export default RecordsContainer;
