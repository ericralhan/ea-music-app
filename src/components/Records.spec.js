import React from 'react';
import { shallow } from 'enzyme';
import { Records } from './Records';

describe('Records component', () => {
  describe('render', () => {
    it('WILL render in empty state', () => {
      const wrapper = shallow(<Records records={{}} />);
      const instance = wrapper.instance();

      expect(instance.props.records).toEqual({});
      expect(wrapper.find('div').length).toEqual(2);
      expect(
        wrapper
          .find('div')
          .at(0)
          .text()
      ).toEqual('Record list:');
      expect(
        wrapper
          .find('div')
          .at(1)
          .text()
      ).toEqual('No records to display');
    });

    it('WILL render with 1 set of records', () => {
      const records = {
        abc: {
          def: {
            ghi: null
          }
        }
      };
      const wrapper = shallow(<Records records={records} />);
      const instance = wrapper.instance();

      expect(instance.props.records).toEqual(records);
      expect(wrapper.find('ul').length).toEqual(3);
      expect(wrapper.find('li').length).toEqual(3);
      expect(
        wrapper
          .find('li')
          .at(2)
          .text()
      ).toEqual('ghi');
    });
  });
});
