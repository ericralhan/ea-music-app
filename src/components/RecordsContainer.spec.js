import React from 'react';
import { shallow } from 'enzyme';
import RecordsContainer from './RecordsContainer';
import * as RecordsTransform from '../helper/RecordsTransform';
import * as FestivalsApi from '../api/FestivalsApi';

jest.mock('./Records');

describe('RecordsContainer component', () => {
  let mockPromise;

  beforeEach(() => {
    const result = [{ foo: 'bar' }];
    mockPromise = Promise.resolve(result);

    FestivalsApi.get = jest.fn(() => mockPromise);
    RecordsTransform.from = jest.fn(() => {});
  });

  describe('render', () => {
    it('WILL render with children', () => {
      const wrapper = shallow(<RecordsContainer />);

      expect(wrapper.find('Records').length).toEqual(1);
    });

    it('WILL pass props to view', () => {
      const wrapper = shallow(<RecordsContainer />);
      const viewComponent = wrapper.find('Records');

      expect(viewComponent.prop('records')).toEqual({});
    });
  });
});
