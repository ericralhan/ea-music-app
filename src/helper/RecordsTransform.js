/**
 * RecordsTransform is a helper class to transform festivals to records.
 *
 * @class RecordsTransform
 */
class RecordsTransform {
  static from(festivals) {
    let records = {};

    festivals.forEach(festival => {
      const festivalName = festival.name || '';
      (festival.bands || []).forEach(band => {
        const recordLabel = band.recordLabel || '';
        const bandName = band.name || '';
        if (!records[recordLabel]) {
          records[recordLabel] = {};
        }
        if (!records[recordLabel][bandName]) {
          records[recordLabel][bandName] = {};
        }

        records[recordLabel][bandName][festivalName] = null;
      });
    });

    return records;
  }
}

export { RecordsTransform };
