import { RecordsTransform } from './RecordsTransform';

describe('RecordsTransform', () => {
  describe('from', () => {
    it('WILL transform empty festivals object to empty records object', () => {
      const festivals = [];

      const records = RecordsTransform.from(festivals);

      expect(records).toEqual({});
    });

    it('WILL transform festivals object to records object', () => {
      const festivals = [
        {
          name: 'LOL-palooza',
          bands: [
            {
              name: 'Winter Primates',
              recordLabel: 'Pacific Records'
            }
          ]
        }
      ];

      const expectedRecords = {
        'Pacific Records': {
          'Winter Primates': {
            'LOL-palooza': null
          }
        }
      };

      const records = RecordsTransform.from(festivals);

      expect(records).toEqual(expectedRecords);
    });
  });
});
