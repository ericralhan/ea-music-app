import { FestivalsApi } from './FestivalsApi';

describe('Festivals Api', () => {
  describe('get', () => {
    it('WILL fetch festivals from api', done => {
      const mockSuccessResponse = {};
      const mockJsonPromise = Promise.resolve(mockSuccessResponse);
      const mockFetchPromise = Promise.resolve({
        json: () => mockJsonPromise
      });
      jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

      FestivalsApi.get();

      expect(global.fetch).toHaveBeenCalledTimes(1);
      expect(global.fetch).toHaveBeenCalledWith(
        FestivalsApi.proxyUrl + FestivalsApi.url
      );

      process.nextTick(() => {
        global.fetch.mockClear();
        done();
      });
    });
  });
});
