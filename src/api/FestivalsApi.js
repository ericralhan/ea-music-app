/**
 * This api calls the url provided by Energy Australia
 * Notes:
 * 1. proxy url is a hack to solve cors issue. There are several ways to solve
 * this issue. I assume this is not the key problem to solve as part of of this
 * coding exercise
 * 2. It can also call a backup (dummy response) api hosted on
 * my-json-server.typicode.com (I added this, as the api provided was not very
 * consistent over the weekend. Just switch the calls to fetch in order to use
 * this)
 *
 * @class FestivalsApi
 */
class FestivalsApi {
  // this is a hack, to get around cors issue
  static proxyUrl = 'https://cors-anywhere.herokuapp.com/';
  static url =
    'http://eacodingtest.digital.energyaustralia.com.au/api/v1/festivals';

  // alternate url when above is not available
  static url1 =
    'http://my-json-server.typicode.com/ericralhan/ea-music-data/festivals';

  static get() {
    // this is a hack, to get around cors issue
    let festivals = fetch(FestivalsApi.proxyUrl + FestivalsApi.url)
      // let festivals = fetch(FestivalsApi.url1)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('response is not ok');
        }
      })
      .then(data => {
        if (data && data !== '') {
          return data;
        } else {
          throw new Error('response is not in expected format');
        }
      })
      .catch(error => {
        console.log(error);
      });

    return festivals;
  }
}

export { FestivalsApi };
